package com.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.main.Log4jDemo;

public class ArithmeticService {
	private static final Logger LOGGER = LogManager.getLogger(Log4jDemo.class);
    public int add(int num1,int num2) {
    	LOGGER.info("Data Received:"+num1 +" "+num2);
    	return num1+num2;
    }

}
