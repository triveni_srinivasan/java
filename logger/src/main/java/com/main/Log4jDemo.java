package com.main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.service.ArithmeticService;

public class Log4jDemo {
	private static final Logger LOGGER = LogManager.getLogger(Log4jDemo.class);

	public static void main(String[] args) {
		
		//DEBUG
		LOGGER.info("Welcome to Log with calculator");
		
		//WARN
	LOGGER.error("In case of exception");
		
		ArithmeticService arithmeticService = new ArithmeticService();
		System.out.println("Addition of Two:"+arithmeticService.add(20, 30));
		LOGGER.info("Log Success");
	
	}
}
