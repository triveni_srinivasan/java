package com.service;

import com.model.Employee;

public interface EmployeeService {

	public static final int VAR = 10;

	public abstract Employee create(Employee employee);

	public abstract Employee readbyEmpId(int empId);

	public abstract Employee[] readbyEmpName(String empName);

	public abstract int update(Employee employee);

	public abstract boolean deletebyEmpId(int empId);

	public abstract boolean deletebyEmpName(String empName);

}
