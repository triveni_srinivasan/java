package com.service;

import com.model.Employee;

public class EmpServiceImpl implements EmployeeService {
//1-Create Employee Records
	@Override
	public Employee create(Employee employee) {
		System.out.println("Service !!Employee ID : " + employee.getEmpId());
		System.out.println("Service !!Employee Name : " + employee.getEmpName());

		return employee;
	}
//2-Read by Employee Id and Employee Name
	@Override
	public Employee readbyEmpId(int empId) {
		Employee employee = null;
		if (empId == 101) {
			employee = new Employee(101, "Tri");

		}
		if (empId == 102) {
			employee = new Employee(102, "Sra");

		}

		return employee;
	}

	@Override
	public Employee[] readbyEmpName(String empName) {
		Employee[] employees = null;
		if (empName.equals("Tri")) {
			employees = new Employee[2];
			Employee employee1 = new Employee(101, "Tri");
			Employee employee2 = new Employee(102, "Tri");
			employees[0] = employee1;
			employees[1] = employee2;
		}

		if (empName.equals("Sra")) {
			employees = new Employee[2];
			Employee employee1 = new Employee(103, "Sra");
			Employee employee2 = new Employee(102, "Tri");
			employees[0] = employee1;
			employees[1] = employee2;
		}

		return employees;
	}

	//Update the employee Deatils
	@Override
	public int update(Employee employee) {
return 0;
	}

	@Override
	public boolean deletebyEmpId(int empId) {
		
		return false;
	}

	@Override
	public boolean deletebyEmpName(String empName) {

		return false;
	}

}
