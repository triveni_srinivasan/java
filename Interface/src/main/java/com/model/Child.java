package com.model;

public class Child extends Parent {
	
	

	public Child(int var) {
		super(var);//Super-Immediate Parent Class
		System.out.println("Child Constructor : " + var);
	
	}

	@Override
	public int add(int num1, int num2) {
		super.add(num1, num2);
		System.out.println("Child Addition  : "+ num1+" " +num2+" "+var);
		return num1+num2+30;
	}

}
