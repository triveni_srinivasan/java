package com.model;

public class Parent extends Object {
	
	protected int var;
	
	//Parameterized Constructor
	public Parent(int var) {
		System.out.println("Parent Constructor : " + var);
		this.var = var;
	}


	public  int add(int num1 , int num2) {
		System.out.println("Parent Addition  : "+ num1+" " +num2);
		return  num1+num2;
		
	}

}
