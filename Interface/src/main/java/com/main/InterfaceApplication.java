package com.main;

import java.util.Scanner;

import com.model.Employee;
import com.service.EmpServiceImpl;
import com.service.EmployeeService;

public class InterfaceApplication {

	public static void main(String[] args) {

		
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter Employee Id :");
		int id = scanner.nextInt();
		System.out.println("Enter Employee Name :");
		String name = scanner.next();

		Employee employeeUI = new Employee(id, name);
		EmployeeService employeeService = new EmpServiceImpl();// injected interface by calling the implement
		// 1-Create Employee Records
		Employee employee = employeeService.create(employeeUI);

		if (employee != null) {
			System.out.println("Welcome :" + employee.getEmpId());
		}

		// 2-Read
		System.out.println("Enter Employee Number to Search ");
		int search = scanner.nextInt();
		Employee employee2 = employeeService.readbyEmpId(search);
		System.out.println();
		if (employee2 != null) {
			System.out.println("Employee Present : " + employee2.getEmpId());
		} else {
			System.out.println("No Employee found!!!!");
		}

		// 2-Read String
		System.out.println("Enter Employee Name to Search ");
		String searchName = scanner.next();
		Employee[] employees1 = employeeService.readbyEmpName(searchName);
		if (employees1!=null && employees1.length > 0) {
			for (int i = 0; i < employees1.length; i++) {
				System.out.println("Employee ID : " + employees1[i].getEmpId());
				System.out.println("Employee Name : " + employees1[i].getEmpName());
			}

		} else {
			System.out.println("No Employee found!!!!");
		}
		
		System.out.println("Enter employee Id to update ");
		int empId=scanner.nextInt();
		int updateEmployee =  employeeService.update(employee2);
		if(updateEmployee ==  empId) {
			System.out.println("Old Employee Id = "+empId);
			employee2 = new Employee(111,"Keerthi");
			
			System.out.println("Updated Employee Id = " +updateEmployee);
		}
		else
		{
			System.out.println("No Employee Found in this ID");
		}
		
		
		
		
		
		scanner.close();
		scanner = null;
		employeeUI = null;
		employee = null;

	}

}
