
package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.exception.LoginException;
import com.model.UserInfo;

public class UserDaoImpl implements UserDao {

	@Override
	public UserInfo createUser(UserInfo userInfo) throws LoginException {
		Connection connection = MySqlConnection.getConnection();
		String sql = "INSERT INTO user(user_id,user_name,password) VALUES(?,?,?)";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, userInfo.getSapId());
			preparedStatement.setString(2, userInfo.getUserName());
			preparedStatement.setString(3, userInfo.getPassword());
			int recordInserted = preparedStatement.executeUpdate();
			if (recordInserted > 0) {
				System.out.println("Inserted Successfully");
			}

			else {
				System.out.println("Not Inserted Successfully");
			}
			connection.close();
		} catch (SQLException e) {
			System.err.println("Sorry!!!No Records are inserted" + e.getMessage());
		}

		return userInfo;
	}

	@Override
	public UserInfo readUserById(int sapId) throws LoginException {
		UserInfo userInfo = null;
		Connection connection = MySqlConnection.getConnection();
		String search = "SELECT * FROM user WHERE user_id=?";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(search);
			preparedStatement.setInt(1, sapId);
			ResultSet resultSet = preparedStatement.executeQuery();
			// iterate
			 userInfo = new UserInfo();
			while (resultSet.next()) {
				// System.out.println(resultSet.getInt("user_id"));
				// System.out.println(resultSet.getString("user_name"));
				// System.out.println(resultSet.getString("password"));
				userInfo.setSapId(resultSet.getInt("user_id"));
				userInfo.setUserName(resultSet.getString("user_name"));
				userInfo.setPassword(resultSet.getString("password"));
			}
		} catch (SQLException e) {

			System.err.println("No ");
		}
		return userInfo;

	}

	@Override
	public UserInfo readUserBySapIdAndPassword(int sapId, String password) throws LoginException {
		// TODO Auto-generated method stub
		return null;
	}

}
