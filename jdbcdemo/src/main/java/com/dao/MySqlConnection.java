package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MySqlConnection {

	public static Connection getConnection() {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
		String driverName = "com.mysql.cj.jdbc.Driver";
		String url = "jdbc:mysql://localhost:3306/java";
		Class.forName(driverName);
		connection = DriverManager.getConnection(url, "root", "TriSachin05.");
		System.out.println(connection != null ? "connection established" : "connection failed");

		} catch (ClassNotFoundException cnfe) {
		System.out.println("There is no respective jars : "
		+ cnfe.getMessage());
		} catch (SQLException se) {// Catching SQL Exception
		System.out.println("SQL Exception :" + se.getMessage());
		} catch (Exception e) {
		System.out.println(e);
		}

		return connection;
		}
	
}
