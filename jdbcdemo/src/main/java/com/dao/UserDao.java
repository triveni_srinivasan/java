
package com.dao;

import com.exception.LoginException;
import com.model.UserInfo;

public interface UserDao {

	public abstract UserInfo createUser(UserInfo userInfo) throws LoginException;

	public abstract UserInfo readUserById(int sapId) throws LoginException;

	public abstract UserInfo readUserBySapIdAndPassword(int sapId, String password) throws LoginException;

	// Update,Del Assignment

}
