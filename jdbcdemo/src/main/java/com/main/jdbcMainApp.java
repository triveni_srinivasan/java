package com.main;

import java.sql.Connection;

import com.dao.MySqlConnection;
import com.dao.UserDao;
import com.dao.UserDaoImpl;
import com.exception.LoginException;
import com.model.UserInfo;

public class jdbcMainApp {

	public static void main(String[] args) {

		Connection connection = MySqlConnection.getConnection();

		UserDao userDao = new UserDaoImpl();
		UserInfo userInfo = new UserInfo();

		/*
		 * userInfo.setSapId(103); 
		 * userInfo.setUserName("Happy");
		 * userInfo.setPassword("Learning");
		 *  try {
		 * UserInfo userInfo2 = userDao.createUser(userInfo);
		 * System.out.println("User Details Saved :" + userInfo2.getUserName()); } catch
		 * (LoginException e) {
		 *  System.err.println(e.getMessage()); 
		 * }
		 */

		try {
			UserInfo userInfo3 = userDao.readUserById(103);
			if (userInfo3 != null) {
System.out.println("Welcome :" +userInfo3.getUserName());
			} else {
				// System.out.println("No user found");
			}
		} catch (LoginException e) {
			System.err.println(e.getMessage());
		}
	}
}
