
package com.model;

import java.io.Serializable;

public class UserInfo implements Serializable {

	private int sapId;
	private String userName;
	private String password;

	public UserInfo() {
		super();

	}

	public UserInfo(String userName, String password) {
			super();
			this.userName = userName;
			this.password = password;
		}

	public int getSapId() {
		return sapId;
	}

	public void setSapId(int sapId) {
		this.sapId = sapId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
