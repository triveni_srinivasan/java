package com.exception;

public class LoginException extends Exception {

	private String message;

	public LoginException(String message) {
		super();
		this.message = message;
	}

	@Override
	public String getMessage() {
		return this.message;
	}
	
	
	
}
