package com.main;

import com.model.ContractEmployee;
import com.model.Employee;
import com.model.PermanentEmployee;

public class MainApplication {

	public static void main(String[] args) {
		
		Employee employee = new ContractEmployee();//parent class =  new child class()
		System.out.println("Contract Employee Salary : "+ employee.parentSalary());
		
		Employee employee1 = new PermanentEmployee();//parent class =  new child class()
		System.out.println("Permanent Employee Salary : "+ employee1.parentSalary());
	}

}
 