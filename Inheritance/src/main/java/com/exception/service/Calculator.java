package com.exception.service;

import java.io.FileNotFoundException;

import com.exception.CustomException;

public class Calculator {
	
	public int add(int num1, int num2) throws CustomException{
		//Validation
		
		if(num1 >0 && num2>0)
		{
			return num1+num2;	
		}
		else {
			throw new CustomException("Please enter Positive  Number");
		}
		
	}

}
