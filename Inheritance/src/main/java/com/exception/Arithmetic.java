package com.exception;

import java.io.FileNotFoundException;
import java.lang.*;

import com.exception.service.Calculator;
import com.exception.service.Myclass;

public class Arithmetic {

	public static void main(String[] args) {
		
		Myclass myClass = new Myclass();
		try {
			String str= myClass.validateUser(123456,"HelloWorld");
			System.out.println(str);
		} catch (CustomException e) {
			System.err.println(e.getMessage());
		}

		/*Calculator calculator = new Calculator();
		int ans=5;
		try {
			ans = calculator.add(10, -0);
		} catch (CustomException e) {
			System.err.println("Solution for negative number : "+e.getMessage());
		}
		System.out.println(ans);*/

		/*
		 * try { int num1 = Integer.parseInt(args[0]); int
		 * num2=Integer.parseInt(args[1]);
		 * 
		 * System.out.println("Division : "+num1/num2); }
		 * 
		 * catch (ArithmeticException | ArrayIndexOutOfBoundsException common) {
		 * System.err.println(" Solution AE " + common.getMessage()); }
		 */

	}

}
