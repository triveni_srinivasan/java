package com.string;

public class StringMain2 {

	public static void main(String[] args) {
		String val1="Hai";
		String str = new String("hai");
		System.out.println(val1);
		System.out.println(str);
		//Compare 2 strings
		if(val1==str)
		{
			System.out.println("Both strings are same");
		}
		else {
			System.out.println("Strings are different");
		}
		
		if(val1.equalsIgnoreCase(str))
		{
			System.out.println("Both strings are same");
		}
		else {
			System.out.println("Strings are different");
		}

	}

}
