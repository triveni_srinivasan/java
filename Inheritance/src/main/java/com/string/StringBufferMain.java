package com.string;

public class StringBufferMain {

	public static void main(String[] args) {
		StringBuffer stringBuffer = new StringBuffer("Hello");
		System.out.println(stringBuffer);
		System.out.println(stringBuffer.capacity());
		stringBuffer.append("World");
		System.out.println(stringBuffer);
		System.out.println("After Update : " + stringBuffer.capacity());
		System.out.println("Length after Update : " + stringBuffer.length());
		StringBuffer stringBuffer2 = new StringBuffer(5);
		System.out.println("String Buffer 2 Capacity :" + stringBuffer.capacity());
		stringBuffer2.append("Hello");
		System.out.println("String Buffer New Capacity :" + stringBuffer.capacity());
		System.out.println(stringBuffer2.reverse());

	}

}
