package com.main;

import java.util.function.BiFunction;

import com.model.Employee;

public class BiFunctionDemo {

	public static void main(String[] args) {
	BiFunction<Integer, String, Employee> biFunction =  (a,b)->{
		return new Employee(a, b);
	};
Employee returnedEmployee = biFunction.apply(10, "Tri");
System.out.println(returnedEmployee.getEmpNo());
	System.out.println(returnedEmployee.getEmpName());
	}

}
