package com.main;

import java.util.ArrayList;
import java.util.List;


public class StreamDemo1 {

	public static void main(String[] args) {
List<String> list = new ArrayList<>();
list.add("Apple");
list.add("Mango");
list.add("Orange");
long ans= list.stream().filter((input)->input.equals("Apple")).count();
System.out.println("How many have the name apple: "+ans);
	

	}

}
