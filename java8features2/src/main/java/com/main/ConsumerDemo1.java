package com.main;

import java.util.function.BiConsumer;

public class ConsumerDemo1 {

	public static void main(String[] args) {
		BiConsumer<Integer, Integer> biConsumer = (a,b) ->{
			System.out.println("Value of a "+a);
			System.out.println("Value of b "+b);
			System.out.println("Addition of a and b "+ (a+b));
			
		};
		biConsumer.accept(22, 23);
			
	}

}
