package com.main;

import com.service.FunctionalProgram;

public class FunctionProgramMain {

	public static void main(String[] args) {
		FunctionalProgram functionalProgram =  new FunctionalProgram();
		String ans=functionalProgram.function((var) ->{return "Hello";});
		System.out.println(ans);
	}

}
