package com.main;

import com.model.Employee;

import com.service.SAMInterface3;

public class LambaExpWithoutParam3 {

	public static void main(String[] args) {

		SAMInterface3 samInterface3 = (Employee employee) -> {
			String str = employee.getEmpName() + "Welcome";
			employee.setEmpName(str);
			return employee;
		};

		samInterface3.display(new Employee(10, "HCL"));
		System.out.println();
		System.out.println();
	}
}
