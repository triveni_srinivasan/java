package com.main;

import com.service.MyClass;
import com.service.MyInterface;

public class MethodReferenceDemo {

	public static void main(String[] args) {
		MyClass myClass =  new MyClass();
		//Reference to an instance method
		MyInterface myInterface = myClass::functionInClass;
		myInterface.display();
		
MyInterface myInterface1= () ->{
			System.out.println("Without Method Reference == ->");
		};
		myInterface1.display();
		
	}

}
