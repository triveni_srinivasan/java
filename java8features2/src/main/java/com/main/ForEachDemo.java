package com.main;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import com.model.Employee;

public class ForEachDemo {

	public static void main(String[] args) {
		Consumer<String > consumer =  (input)->{
			System.out.println("Welcome "+input);
		};
		Employee employee1 = new Employee(1,"One");
		Employee employee2 = new Employee(2,"Two");
		Employee employee3 = new Employee(3,"Three");
		Employee employee4 = new Employee(4,"Four");
		List<Employee> collectionOfEmployee = new ArrayList<>();
		collectionOfEmployee.add(employee1);
		collectionOfEmployee.add(employee2);
		collectionOfEmployee.add(employee3);
		collectionOfEmployee.add(employee4);
		
		//collectionOfEmployee.forEach(employee)()->{
	//	System.out.println(emp.g);
		//System.out.println();
	//}
		}


}
