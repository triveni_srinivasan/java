package com.service;

@FunctionalInterface
public interface SAMInterface {

	public abstract void display();

	public default void print() {
		System.out.println("Deafault");
	}

	public static void staticMethod() {
		System.out.println("Static");
	}

}
