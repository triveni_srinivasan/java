package com.main;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.model.Department;
import com.model.Student;
import com.service.StudentServiceImpl;

public class CollegeMain {

	public static void main(String[] args) {
		Student student1 = new Student(1, "Tri", 22, 90);
		Student student2 = new Student(2, "priya", 21, 95);
		Student student3 = new Student(3, "Sravani", 23, 99);
		Student student4 = new Student(4, "Arun", 25, 100);
		

		Department department = new Department();
		department.setDeptId(1);
		department.setDeptName("CSE");

		Set<Student> students = new HashSet<>();
		students.add(student1);
		students.add(student2);
		students.add(student3);
		students.add(student4);
		

		department.setStudents(students);
		
		Department department2 = new Department();
		department2.setDeptId(2);
		department2.setDeptName("ECE");
		
		Set<Student> students2 = new HashSet<>();
		students2.add(student3);
		students2.add(student4);
		
		department2.setStudents(students2);
		
		

		StudentServiceImpl impl = new StudentServiceImpl();
		List<Student> fetch = impl.searchByName(department, "Arun");
		if (fetch != null && fetch.size() > 0) {
			for (Student student : fetch) {
				System.out.println("Searching By Name:");
				System.out.println("Student Number:" + student.getStudId());
				System.out.println("Student Name:" + student.getStudName());
				System.out.println("Student Name:" + student.getStudAge());
				System.out.println("\n");
			}

		} else {
			System.out.println("NO records found!!!");
		}
		List<Student> fetch1 = impl.searchByAge(department, 25);
		if (fetch1 != null && fetch1.size() > 0) {
			for (Student student : fetch1) {
				System.out.println("Searching By Age:");
				System.out.println("Student Number:" + student.getStudId());
				System.out.println("Student Name:" + student.getStudName());
				System.out.println("Student Name:" + student.getStudAge());
				System.out.println("\n");
			}

		} else {
			System.out.println("NO records found!!!");
		}
		List<Student> fetch2 = impl.searchByScore(department, 100);
		if (fetch2 != null && fetch2.size() > 0) {
			for (Student student : fetch1) {
				System.out.println("Searching By Score:");
				System.out.println("Student Number:" + student.getStudId());
				System.out.println("Student Name:" + student.getStudName());
				System.out.println("Student Name:" + student.getStudAge());
			}

		} else {
			System.out.println("NO records found!!!");
		}

	}

}
