package com.model;

public class Student {
	private int studId;
	private String studName;
	private int studAge;
	private int studMarks;

	public Student() {
		super();
	}

	public Student(int studId, String studName, int studAge, int studMarks) {
		super();
		this.studId = studId;
		this.studName = studName;
		this.studAge = studAge;
		this.studMarks = studMarks;
	}

	public int getStudId() {
		return studId;
	}

	public void setStudId(int studId) {
		this.studId = studId;
	}

	public String getStudName() {
		return studName;
	}

	public void setStudName(String studName) {
		this.studName = studName;
	}

	public int getStudAge() {
		return studAge;
	}

	public void setStudAge(int studAge) {
		this.studAge = studAge;
	}

	public int getStudMarks() {
		return studMarks;
	}

	public void setStudMarks(int studMarks) {
		this.studMarks = studMarks;
	}

}
