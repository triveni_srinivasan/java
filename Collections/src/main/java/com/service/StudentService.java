package com.service;

import java.util.List;


import com.model.Department;
import com.model.Student;

public interface StudentService {
	public abstract List<Student> searchByName(Department department, String name);

	public abstract List<Student> searchByAge(Department department, int age);

	public abstract List<Student> searchByScore(Department department, int score);
	
	//public abstract List<Department> searchByNameEachDept(Set<Department> departments, String name);
}
