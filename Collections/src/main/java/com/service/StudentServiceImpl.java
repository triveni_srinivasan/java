package com.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.model.Department;
import com.model.Student;

public class StudentServiceImpl implements StudentService {

	@Override
	public List<Student> searchByName(Department department, String name) {
		List<Student> list = new ArrayList<>();
		if (department != null && name.length() > 0) {
			Set<Student> group = department.getStudents();
			for (Student student : group) {
				if (name.equals(student.getStudName())) {
					list.add(student);
				}
			}
		}
		return list;
	}

	@Override
	public List<Student> searchByAge(Department department, int age) {
		List<Student> list = new ArrayList<>();
		if (department != null && age > 0) {
			Set<Student> group = department.getStudents();
			for (Student student : group) {
				if (age == student.getStudAge()) {
					list.add(student);
				}
			}
		}
		return list;
	}

	@Override
	public List<Student> searchByScore(Department department, int score) {
		List<Student> list = new ArrayList<>();
		if (department != null && score > 0) {
			Set<Student> group = department.getStudents();
			for (Student student : group) {
				if (score == student.getStudMarks()) {
					list.add(student);
				}
			}
		}
		return list;
	}

	

}
