package com.model;

import java.util.Comparator;

public class Employee  {
	
	private int empNo;
	private String empName;
	private int age;
	private int salary;
	
	
	
	public Employee() {
		super();
		
	}
	public Employee(int empNo, String empName, int age, int salary) {
		super();
		this.empNo = empNo;
		this.empName = empName;
		this.age = age;
		this.salary = salary;
	}
	public int getEmpNo() {
		return empNo;
	}
	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	
		
}
