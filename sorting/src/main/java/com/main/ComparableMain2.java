package com.main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.model.Employee;

public class ComparableMain2 {

	public static void main(String[] args) {
		Employee employee1 = new Employee(10, "One", 22, 1010);	
		Employee employee2 = new Employee(20, "Two", 25, 2020);
		Employee employee3 = new Employee(30, "Three", 23, 3030);
		Employee employee4 = new Employee(40, "Four", 24, 4040);

		List<Employee> employees = new ArrayList();
		employees.add(employee1);
		employees.add(employee2);
		employees.add(employee3);
		employees.add(employee4);
	//	Collections.sort(employees);

		// Iteration

		for (Employee employee : employees) {
			System.out.println("Employee No " + employee.getEmpNo());
			System.out.println("Employee Name " + employee.getEmpName());
			System.out.println("Employee Age " + employee.getAge());
			System.out.println("Employee Salary " + employee.getSalary());
		}
	}

}
