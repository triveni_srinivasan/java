package com.service;

import java.util.Comparator;

import com.model.Employee;

public class AgeCompare implements Comparator<Employee> {

	@Override
	public int compare(Employee e1, Employee e2) {
		
		return e1.getAge() -  e2.getAge() ;
	}
	

}
