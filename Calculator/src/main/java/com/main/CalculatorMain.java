package com.main;

import java.util.Scanner;

import com.service.CalculatorService;
import com.service.CalculatorServiceImpl;

public class CalculatorMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter Number1");
		int num1 = scanner.nextInt();
		System.out.println("Enter Number2");
		int num2 = scanner.nextInt();

		CalculatorService calculatorservice = new CalculatorServiceImpl();
		System.out.println("Addition of " + num1 + "+" + num2 + "=" + calculatorservice.add(num1, num2));
		System.out.println("Subtraction of " + num1 + "-" + num2 + "=" + calculatorservice.sub(num1, num2));
		System.out.println("Muliplication of " + num1 + "*" + num2 + "=" + calculatorservice.mul(num1, num2));
		System.out.println("Division of " + num1 + "/" + num2 + "=" + calculatorservice.div(num1, num2));
		System.out.println("Modulo of " + num1 + "/" + num2 + "=" + calculatorservice.mod(num1, num2));

		// Scientific Calculator

		System.out.println("Enter the value ");
		double number = scanner.nextDouble();

		System.out.println("Sin Value of " + number + "=" + calculatorservice.sin(number));
		System.out.println("%.4d" + calculatorservice.sin(number));
		System.out.println("Cos Value of " + number + "=" + calculatorservice.cos(number));
		System.out.println("Tan Value of " + number + "=" + calculatorservice.tan(number));

	}

}
