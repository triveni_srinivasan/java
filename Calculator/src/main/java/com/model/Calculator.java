package com.model;

public class Calculator {

	private int num1, num2;
	private double number;

	public Calculator() {
		super();
	}
	// TODO Auto-generated constructor stub

	public Calculator(int num1, int num2) {
		super();
		this.num1 = num1;
		this.num2 = num2;
	}

	public Calculator(float number) {
		super();
		this.number = number;
	}

	public int getNum1() {
		return num1;
	}

	public void setNum1(int num1) {
		this.num1 = num1;
	}

	public int getNum2() {
		return num2;
	}

	public void setNum2(int num2) {
		this.num2 = num2;
	}

	public double getNumber() {
		return number;
	}

	public void setNumber(double number) {
		this.number = number;
	}

}
