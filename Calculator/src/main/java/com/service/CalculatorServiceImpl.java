package com.service;

public class CalculatorServiceImpl implements CalculatorService {

	@Override
	public int add(int num1, int num2) {

		return num1 + num2;
	}

	@Override
	public int sub(int num1, int num2) {

		return num1 - num2;
	}

	@Override
	public int mul(int num1, int num2) {
		return num1 * num2;
	}

	@Override
	public int div(int num1, int num2) {

		return num1 / num2;
	}

	@Override
	public int mod(int num1, int num2) {

		return num1 % num2;
	}
	// Scientific Calculator

	@Override
	public double sin(double number) {
		double radians = Math.toRadians(number);
		double valueOfSin = Math.sin(radians);
		return valueOfSin;
	}

	@Override
	public double cos(double number) {
		double radians = Math.toRadians(number);
		double valueOfCos = Math.cos(radians);
		return valueOfCos;
	}

	@Override
	public double tan(double number) {

		double radians = Math.toRadians(number);
		double valueOfTan = Math.tan(radians);
		return valueOfTan;
	}

}
