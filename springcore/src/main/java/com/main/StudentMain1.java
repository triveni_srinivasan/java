package com.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.model.Student;

public class StudentMain1 {
public static void main (String[] args){
//Loading the xml file
	
ApplicationContext applicationContext =  new ClassPathXmlApplicationContext("com/config/springconfig.xml");

applicationContext.getBean("student");
//Read the bean based on the xml file
Student student = (Student) applicationContext.getBean("student");


System.out.println(student.getStuId());
System.out.println(student.getStuName());
System.out.println(student.getAge());
System.out.println(student.getMarks());
Student student2 =  new Student();
student2 = null;
System.out.println("End");

	
}
}
