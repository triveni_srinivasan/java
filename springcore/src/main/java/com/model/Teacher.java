package com.model;

public class Teacher {

	private int TeacherId;
	private String TeacherName;
	
	public Teacher() {
		System.out.println("Default :Teacher Cons");
	}
	
	public Teacher(int teacherId, String teacherName) {
		super();
		TeacherId = teacherId;
		TeacherName = teacherName;
	}
	public int getTeacherId() {
		return TeacherId;
	}
	public void setTeacherId(int teacherId) {
		TeacherId = teacherId;
	}
	public String getTeacherName() {
		return TeacherName;
	}
	public void setTeacherName(String teacherName) {
		TeacherName = teacherName;
	}
	
}
