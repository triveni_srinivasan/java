package com.model;


public class Student {

	private int stuId;
	private String stuName;
	private int age;
	private int marks;
	
	public Student() {
		super();
		//System.out.println("Default :Student Construcor");
	}
	public Student(int stuId, String stuName, int age, int marks) {
		super();
		this.stuId = stuId;
		this.stuName = stuName;
		this.age = age;
		this.marks = marks;
	}
	public Student(int stuId, String stuName) {
		super();
		this.stuId = stuId;
		this.stuName = stuName;
		System.out.println("2 Param");
	}
	public int getStuId() {
		return stuId;
	}
	public void setStuId(int stuId) {
		this.stuId = stuId;
	}
	public String getStuName() {
		return stuName;
	}
	public void setStuName(String stuName) {
		this.stuName = stuName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getMarks() {
		return marks;
	}
	public void setMarks(int marks) {
		this.marks = marks;
	}
	
	
	
}
