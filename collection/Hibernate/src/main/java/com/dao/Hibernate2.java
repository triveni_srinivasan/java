package com.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.model.Employee;

/**
 * This is to demostrate Get and load in hibernate
 * 
 * @author ks.venkatsai
 *
 */
public class Hibernate2 {
	public static void main(String[] args) {
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.xml").build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction(); // commit & rollback
		Employee employee = session.get(Employee.class, 2);
		// Same way we can use load which is same but load gives Null found exception
		if (employee != null) {
			System.out.println("Empno:" + employee.getEmpNo());
			System.out.println("Empno:" + employee.getEmpName());
		} else {
			System.out.println("Not found Employee");
		}
		session.save(employee);
		transaction.commit(); // persist
		System.out.println("successfully saved");

		session.close();
		factory.close();
	}
}
