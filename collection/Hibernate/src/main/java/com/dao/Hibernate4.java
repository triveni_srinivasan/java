package com.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.model.Employee;

/**
 * This is to demonstrate how to update a value in db using hibernate
 * 
 * @author ks.venkatsai
 *
 */
public class Hibernate4 {
	public static void main(String[] args) {
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("hibernate.xml")
		.build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction(); // commit & rollback
		Employee employee = session.get(Employee.class, 2);
		session.saveOrUpdate(employee);
		if(employee!=null) {
			employee.setEmpNo(2);
			employee.setEmpName("Two");
		}
		transaction.commit(); // persist
		System.out.println("successfully saved");

		session.close();
		factory.close();
		}
}
