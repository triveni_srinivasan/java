package com.main;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;


import com.model.User;

public class UserSetDemo {

	public static void main(String[] args) {
		
		User user1 = new User(10, "Hello", "World");
		User user2 = new User(10, "Good", "Morning");
		User user3 = new User(10, "Thank", "You");

		User[] userArray = new User[3];// Array--Fixed Memory Allocation

		// dynamic
		// Interface interface = new InterfaceImpl();//---Abstraction

		List<User> list = new ArrayList<>();
		list.add(user1);
		list.add(user2);
		list.add(user3);
		list.add(user1);
		//list.add(new Integer(12));  //convert primitive type to object
		//list.add(33);
		
		System.out.println(list.indexOf(user2));
		System.out.println(list.size());

		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			User user11 = (User) iterator.next();
			System.out.println("User Id" + user11.getUserId());
			System.out.println("User Name:" + user11.getUserName());
			System.out.println("User Password:" + user11.getPassword());

		}
		list.remove(3);

	}

}
