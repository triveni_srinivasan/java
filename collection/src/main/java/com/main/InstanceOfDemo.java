package com.main;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class InstanceOfDemo {

	public static void main(String[] args) {
		Set set = new HashSet();
		set.add("Hello");
		set.add("Good");
		set.add("Thanks");
		set.add(new Integer(12));
		System.out.println(set.size());//wrapper primitive to object
		set.add(33);//autoboxing
		
		//iterate to display
		for (Iterator iterator = set.iterator(); iterator.hasNext();) {
			Object object = (Object) iterator.next();
			if (object instanceof String) {
				String new_name = (String) object;
				System.out.println(new_name);
			}
			if (object instanceof Integer) {
				int new_name = (int) object;
				System.out.println(new_name);
			}
			
			
		}
		

	}

}
