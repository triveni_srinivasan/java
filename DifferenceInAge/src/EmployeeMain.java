
public class EmployeeMain {

	public static void main(String[] args) {
		
		Employee employee1 = new Employee();
		
		System.out.println("Employee 1 Memory Address : " + employee1);
		employee1.empId   = 1;
		employee1.empName = "Tri";
		employee1.age     = 22;
		employee1.salary  = 2020.0f;
		
		employee1.display();
		
        
        Employee employee2 = new Employee();
		
		employee2.empId   = 2;
		employee2.empName = "Bowiya";
		employee2.age     = 23;
		employee2.salary  = 2022.0f;
		System.out.println("\nEmployee 2 Memory Address : " + employee2);
		
		employee2.display();
		
		EmployeeService employeeService =  new EmployeeService();
		String Result = employeeService.differenceInAge(employee1.age,employee2.age);
		System.out.println(Result);
		employee1 = null;
		employee2 = null;
		employeeService = null;
		
		}

}
