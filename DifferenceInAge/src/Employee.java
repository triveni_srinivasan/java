
public class Employee {
	
	int empId;
	String empName;
	int age;
	float salary;
	
	void display() {
		System.out.println("Employee ID     : " + this.empId);
		System.out.println("Employee Name   : " + this.empName);
		System.out.println("Employee Age    : " + this.age);
		System.out.println("Employee Salary : " + this.salary);
	}

}
