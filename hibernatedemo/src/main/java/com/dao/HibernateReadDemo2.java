package com.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.model.Employee;

public class HibernateReadDemo2 {

	public static void main(String[] args) {

			/*StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("config/hibernate.cfg.xml")
					.build();*/
			StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure()
					.build();
			Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
			SessionFactory factory = meta.getSessionFactoryBuilder().build();
			Session session = factory.openSession();
		
			Employee employee = session.load(Employee.class, 10);
if(employee!=null)
{
	System.out.println("Employee num: "+employee.getEmpNo());
	System.out.println("Employee Name:"+employee.getEmpName());;;
}
else {
	System.out.println("No employee found");
}
	
			System.out.println("Read saved");

			session.close();
			factory.close();
		}

	

}
