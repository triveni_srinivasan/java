package com.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.model.Employee;

public class HibernateDemo {
	
	public static void main(String[] args) {

			/*StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("config/hibernate.cfg.xml")
					.build();*/
			StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure()
					.build();
			Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
			SessionFactory factory = meta.getSessionFactoryBuilder().build();
			Session session = factory.openSession();
			Transaction transaction = session.beginTransaction(); // commit & rollback

			// Logic
			Employee employee = new Employee(101, "Tri");
			session.save(employee);

			transaction.commit(); 
			//transaction.rollback();// persist
			System.out.println("successfully saved");

			session.close();
			factory.close();
		}

	}

