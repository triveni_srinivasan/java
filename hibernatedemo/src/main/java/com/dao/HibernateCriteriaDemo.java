package com.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.model.Employee;
import com.mysql.cj.x.protobuf.MysqlxCrud.CreateViewOrBuilder;

public class HibernateCriteriaDemo {

	public static void main(String[] args) {

		/*
		 * StandardServiceRegistry ssr = new
		 * StandardServiceRegistryBuilder().configure("config/hibernate.cfg.xml")
		 * .build();
		 */
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure().build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();

		javax.persistence.criteria.CriteriaQuery<Employee> cq = session.getCriteriaBuilder().createQuery(Employee.class);
		cq.from(Employee.class);
		session.createQuery(cq).getResultList();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		javax.persistence.criteria.CriteriaQuery<Employee> query = builder.createQuery(Employee.class);
		Root<Employee> root = (Root) query.from(Employee.class);
query.select(root.get("empName"));

		Query query1 = session.createQuery(query);
		List<String> list = query1.getResultList();
		for (String name : list) {
			System.out.println(name);
		}
			
		
		session.close();
		factory.close();
	}

}
