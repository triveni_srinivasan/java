package com.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.model.Employee;

public class HibernateHQLDemo3 {

	public static void main(String[] args) {

			/*StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure("config/hibernate.cfg.xml")
					.build();*/
			StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure()
					.build();
			Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
			SessionFactory factory = meta.getSessionFactoryBuilder().build();
			Session session = factory.openSession();
		
			String hqlRead = "from Employee";
			Query<Employee>  query = session.createQuery(hqlRead);
			List<Employee> listOfEmployees = query.list();
			for (Employee employee : listOfEmployees) {
				System.out.println("Employee Num :"+employee.getEmpNo());
				System.out.println("Employee Name"+employee.getEmpName());;
			}
			System.out.println("successfully saved");

			session.close();
			factory.close();
		}

	

}
