package com.dao;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.model.Department;
import com.model.Employee;


//perform native query

public class HibernateDemo {

	public static void main(String[] args) {

		/*
		 * StandardServiceRegistry ssr = new
		 * StandardServiceRegistryBuilder().configure("config/hibernate.cfg.xml")
		 * .build();
		 */
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure().build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();

		Department department = new Department();
		department.setDeptName("Developer");
		session.save(department);
		Transaction transaction = session.beginTransaction();

		Employee employee1 = new Employee("Tri", "triveni@email.com", department);
		session.save(employee1);
		Employee employee2 = new Employee("Priya", "priya@email.com", department);
		session.save(employee2);
		Employee employee3 = new Employee("Sra", "sra@email.com", department);
		session.save(employee3);
		
		Set<Employee> employees = new HashSet<>();
		employees.add(employee1);
		employees.add(employee2);
		employees.add(employee3);

		department.setEmployees(employees);
		session.save(department);
		transaction.commit();
		
		System.out.println("successfully saved");

		session.close();
		factory.close();
	}

}
