package com.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "deptonetomany")
public class Department implements Serializable {
	@Id
//@GeneratedValue
	@Column(name = "dept_id")
	private int deptId;
	@Column(name = "dept_name", length = 25)
	private String deptName;
	@OneToMany(mappedBy="department",cascade=CascadeType.ALL)
	private Set<Employee> employees;

	public Department() {
		super();
	}

	public Department(String deptName, Set<Employee> employees) {
		super();
		this.deptName = deptName;
		this.employees = employees;
	}

	public int getDeptId() {
		return deptId;
	}

	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public Set<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(Set<Employee> employees) {
		this.employees = employees;
	}

}
