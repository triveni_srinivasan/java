package com.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
//Mapped to  the  Db - using entity

@Entity
@Table(name = "studenttable")
@NamedQueries(
		{
			@NamedQuery(
					name="findStudentByFName",
					query="from Student student where student.firstName=:falias"
			
			),
              @NamedQuery(
		            name="findStudentByLName",
		            query="from Student student where student.lastName=:lalias"

            )

		})
		
	

public class Student implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "studentid")

	private int stuId;
	@Column(name = "first_name", length = 25)
	private String firstName;
	@Column(name = "last_name", length = 25)
	private String lastName;
	@Column(name = "Email", length = 25)
	private String email;

	public Student() {
		super();
	}

	public Student(String firstName, String lastName, String email) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}

	public int getStuId() {
		return stuId;
	}

	public void setStuId(int stuId) {
		this.stuId = stuId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
