package com.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import com.model.Student;

public class HibernateDemo2 {

	public static void main(String[] args) {

		/*
		 * StandardServiceRegistry ssr = new
		 * StandardServiceRegistryBuilder().configure("config/hibernate.cfg.xml")
		 * .build();
		 */
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure().build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();
		//Read data
		Query query =  session.getNamedQuery("findStudentByFName");
		query.setParameter("falias", "Hello");
		List<Student> students =  query.getResultList();
		for (Student student : students) {
			System.out.println(student.getStuId());
			System.out.println(student.getFirstName());
			System.out.println(student.getLastName());
		}
		System.out.println("Read by FirstName");

		session.close();
		factory.close();
	}

}
