package com.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import com.model.Student;

public class NativeQuery {

	public static void main(String[] args) {

		/*
		 * StandardServiceRegistry ssr = new
		 * StandardServiceRegistryBuilder().configure("config/hibernate.cfg.xml")
		 * .build();
		 */
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure().build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction(); // commit & rollback

		Student student = new Student("Hello", "World", "helloworld@email.com");
		session.save(student);

		transaction.commit();
		System.out.println("successfully saved");

		session.close();
		factory.close();
	}

}
