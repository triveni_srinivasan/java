package com.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import com.model.Student;

//perform native query

public class HibernateDemo {

	public static void main(String[] args) {

		/*
		 * StandardServiceRegistry ssr = new
		 * StandardServiceRegistryBuilder().configure("config/hibernate.cfg.xml")
		 * .build();
		 */
		StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().configure().build();
		Metadata meta = new MetadataSources(ssr).getMetadataBuilder().build();
		SessionFactory factory = meta.getSessionFactoryBuilder().build();
		Session session = factory.openSession();
		String nativeQuery = "SELECT * FROM studenttable";
		List<Student> students = session.createNativeQuery(nativeQuery).addEntity(Student.class).list();
		for (Student student : students) {
			System.out.println(student.getFirstName());
		}
		System.out.println("successfully saved");

		session.close();
		factory.close();
	}

}
