package com.main;

import java.util.Scanner;

import com.model.Employee;

public class CommandLine {

	public static void main(String[] args) {

		/*
		 * Command Line 
		 * String data1 =args[0]; System.out.println(data1);//convert
		 * string to int
		 * 
		 * //Wrapper Class int ans = Integer.parseInt(data1);
		 * System.out.println(100+ans);
		 */
		
		Scanner scanner =  new Scanner(System.in);
		System.out.println("Enter Employee Id : ");
		int id=scanner.nextInt();
		
		
		System.out.println("Enter Employee Name :");
		String name=scanner.next();
		
		
		System.out.println("Enter Employee Salary :")	;
		float salary =scanner.nextFloat();
		
		System.out.println("Enter Employee Age :")	;
		int age =scanner.nextInt();
				
		Employee employee = new Employee(id, name, salary, age);
		
		employee.setEmpId(id);
		employee.setEmpName(name);
		employee.setSalary(salary);
		employee.setAge(age);
		
		System.out.println("Employee Id : "+employee.getEmpId());
		System.out.println("Employee Name : "+employee.getEmpName());
		System.out.println("Employee Salary : "+employee.getSalary());
		System.out.println("Employee Age : "+employee.getAge());
		

	}

}
