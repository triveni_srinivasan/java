package com.main;

import com.model.Employee;

public class MainApplication {

	public static void main(String[] args) {

		Employee employee = new Employee(101, "Triveni ", 10101f, 22);

		System.out.println("Employee ID : " + employee.getEmpId());

		System.out.println("Employee Name : " + employee.getEmpName());

		System.out.println("Employee Salary : " + employee.getSalary());

		System.out.println("Employee Age : " + employee.getAge());

		employee.setAge(23);
		employee.setEmpName("Sravani");

		System.out.println("\nEmployee ID : " + employee.getEmpId());

		System.out.println("Employee Name : " + employee.getEmpName());

		System.out.println("Employee Age : " + employee.getAge());

		System.out.println("Employee Salary : " + employee.getSalary());
	}
}
