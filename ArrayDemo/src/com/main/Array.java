package com.main;

import com.service.BusinessService;

import java.util.Scanner;

public class Array {

	public static void main(String[] args) {

		int intVar[] = { 1, 2, 3, 4 };
		BusinessService businessService = new BusinessService();
		System.out.println("\nSum of Array Values : " + businessService.sumValues(intVar));

		System.out.println("\nBiggest Number in Array : " + businessService.biggestNumber(intVar));

		int searchNumber = 6;
		boolean result = businessService.searchNumber(intVar, 6);
		if (result == true)
			System.out.println("\nElement  found " + searchNumber);
		else
			System.out.println("\nElement not found " + searchNumber);
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the number to check whether it's odd or even ");
		int input=scan.nextInt();
		System.out.println("The given number is an "+ businessService.oddOrEven(input));
		
		Scanner in = new Scanner(System.in);
		System.out.println("Enter the number to check whether it's prime or not ");
		int num=in.nextInt();
		System.out.println("The given number is  "+ businessService.primeNumber(num));
		
		
			

	}

}
