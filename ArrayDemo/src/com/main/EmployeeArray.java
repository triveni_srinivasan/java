package com.main;

import java.util.Scanner;

import com.model.Employee;
import com.service.EmpService;

public class EmployeeArray {

	public static void main(String[] args) {

		Employee emp1 = new Employee(1001, "Triveni ", 20000f, 22);
		Employee emp2 = new Employee(1002, "Sravani ", 30000f, 22);
		Employee emp3 = new Employee(1003, "Priya ", 40000f, 20);
		Employee emp4 = new Employee(1004, "Keerthi ", 50000f, 19);

		// collecion of employee
		Employee[] employees = new Employee[4];
		employees[0] = emp1;
		employees[1] = emp2;
		employees[2] = emp3;
		employees[3] = emp4;

		// search for an employeeid
		EmpService empService = new EmpService();
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the employee Id to check ");
		int id = scan.nextInt();

		Employee employee = empService.searchEmpId(employees, id);
		if (employee != null) {
			System.out.println("Id  " + employee.getEmpId() + " Employee is present");
		} else
			System.out.println("Sorry!!!  Employee Not found");

		// search by age and display that employee details
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the employee Age to check ");
		int age = sc.nextInt();

		Employee[] employe = empService.searchEmpAge(employees, age);
		if (employe.length > 0) {
			for (int i = 0; i < employe.length; i++) {
				System.out.println("\nEmployee Name " + employe[i].getEmpName());
				System.out.println("\nAge" + employe[i].getAge());
			}

		} else
			System.out.println("Sorry!!!  Employee Not found with that age ");
	}
}
