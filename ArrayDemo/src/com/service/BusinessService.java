package com.service;

public class BusinessService {

	// 1) Sum of array values
	public int sumValues(int[] array) {
		int temp = 0;
		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
			temp += array[i];

		}
		return temp;
	}

	//2) find the biggest number in an array
	public int biggestNumber(int[] array) {
		int tempVar = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] > tempVar) {
				tempVar = array[i];
			}
		}
		return tempVar;
	}

	// 3) Search particular element in an array
	public boolean searchNumber(int[] array, int search) {
		int dummy = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] == search) {
				dummy = array[i];
			}
		}
		if (dummy > 0) {
			return true;
		} else {
			return false;
		}

	}
	 
	// 4) check whether the given number is odd or even-Assignment -2 !!!
	
	
	public String oddOrEven(int number) {
		if(number%2==0)
			return "Even Number";
			else
				return "Odd Number";
	}
	//5)Check whether the given number is prime or not- Assignment -2 !!!!
	public String primeNumber(int number) {
		int count=0;
		for(int i=1;i<=number;i++)
		{
			if(number%i==0)
			{
				count++;
				//System.out.println(count);
			}
		}
		if(count==2)
			return "Prime";
		else
			return "Not a Prime ";
	}
	
}
