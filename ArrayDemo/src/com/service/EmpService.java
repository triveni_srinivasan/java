package com.service;

import com.model.Employee;

public class EmpService {

	// iterate through an employee array and find the empId-using object (employee)

	public Employee searchEmpId(Employee[] empArray, int empId) {

		Employee tempEmployee = null;

		for (int i = 0; i < empArray.length; i++) {
			if (empArray[i].getEmpId() == empId) {
				tempEmployee = empArray[i];

			} else {
				// NA
			}

		}
		return tempEmployee;

	}
	// Search by age

	public Employee[] searchEmpAge(Employee[] empArray, int age) {

		Employee[] tempEmployee = new Employee[2];

		int count =0;
		 
		for (int i = 0; i < empArray.length; i++) {
			if (empArray[i].getAge() == age) {
					tempEmployee[count] = empArray[i];
					count++;

			} else 
			{
				// NA
			}
				
		}
		return tempEmployee;

	}
}
