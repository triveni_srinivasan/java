package com.service;

public class Static {
	// class level
	public static int var = 10;
	
	private float var1=2.3f;

	// method level
	public static void display() {
		Static staticobj = new Static();
		
		System.out.println("Static Method"+ staticobj.var);
	}

}
 