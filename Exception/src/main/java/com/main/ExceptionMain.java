
package com.main;

import java.util.Scanner;

import javax.xml.bind.ValidationException;

import com.model.Validation;
import com.service.UserValidation;

public class ExceptionMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		
		System.out.println("ENTER SAP ID");
		int sapId = scanner.nextInt();
		System.out.println("ENTER USER NAME");
		String userName = scanner.next();
		System.out.println("ENTER PASSWORD");
		String password = scanner.next();

		UserValidation userValidation = new UserValidation();

		try {

			String str = userValidation.Validation(sapId, userName, password);
			System.out.println(str);

		} catch (ValidationException e) {
			System.err.println(e.getMessage());
		}
		scanner.close();

	}

}