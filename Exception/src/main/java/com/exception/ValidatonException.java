package com.exception;

public class ValidatonException extends Exception {
	private String message;

	public ValidatonException(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	

	

}
