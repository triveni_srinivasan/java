package com.model;



public class Validation {
	private int sapId;
	private String userName,password;
	
	public Validation() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Validation(int sapId, String userName, String password) {
		super();
		this.sapId = sapId;
		this.userName = userName;
		this.password = password;
	}
	public int getSapId() {
		return sapId;
	}
	public void setSapId(int sapId) {
		this.sapId = sapId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	

}
